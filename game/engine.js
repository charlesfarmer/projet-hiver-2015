var ChainableError = requireBase("utils/ChainableError");
var util = require("util");

function GameEngine(models){
    this.className = "GameEngine";
    this.models = models;
    this.Promise = this.models.Sequelize.Promise;
    this.sequelize = this.models.sequelize;
}
var methods = GameEngine.prototype;

function GameError(message) {
  GameError.super_.call(this, message);
  this.name = "GameError";
}
util.inherits(GameError, ChainableError);
GameEngine.Error = GameError;

methods.create = function(lobby){
    var games = this.models.game;
    var Promise = this.Promise;
    var rivals = lobby.getUsers();
    var options = lobby.getGameOption();

    return Promise.join(rivals, options, function(rivals, options){
        if(rivals.length < options.player_min){
            throw new GameError("missing.users"); }
        return games.create({ lobbyId: lobby.id });
    });
}

methods.initialize = function(game){
    var players = this.models.player;
    return game.getLobby()
    .then(function(lobby){
            return lobby.getUsers();
    })
    .then(function(users){
        var playerData = [];
        users.forEach(function(user){
            playerData.push({
                userId: user.getDataValue('id'),
                gameId: game.getDataValue('id'),
                points: 0
            });
        });
        return players.bulkCreate(playerData);
    })
    .then(function(){
        return game;
    });
};

methods.addRound = function(game){
    var rounds = this.models.round;
    var players = this.models.player;
    var q_cards = this.models.q_card;
    var Promise = this.Promise;
    var Sequelize = this.models.Sequelize;

    var paramsCzar             = {};
        paramsCzar.where       = { gameId: game.id };
        paramsCzar.order       = [ Sequelize.fn('RAND') ];
    var paramsCards            = {};
        paramsCards.where      = { gameId: game.id, questionId: { $not: null } };
        paramsCards.attributes = [ "questionId" ];

    var newCzar           = players.find(paramsCzar);
    var previousQuestions = rounds.findAll(paramsCards);

    return Promise.join(newCzar, previousQuestions, function(newCzar, previousQuestions){
        var previousQCardIds  = previousQuestions.map(function(c){ return c.questionId; });
        var paramsQCard       = {};
            paramsQCard.where = ( previousQCardIds.length > 0 ) ? { id: { $not: previousQCardIds } } : {};
            paramsQCard.order = [ Sequelize.fn('RAND') ];
        return Promise.all([newCzar, q_cards.find(paramsQCard)]);
    })
    .spread(function(czar, question){
            if( ! question ){ throw new GameError("missing.roundQuestion"); }
            if( ! czar     ){ throw new GameError("missing.czar")}
            var newRound            = {};
                newRound.gameId     = game.id;
                newRound.czarId     = czar.id;
                newRound.questionId = question.id;
            return rounds.create(newRound);
    });
};

methods.fillHands = function(game){
    var games       = this.models.game;
    var hands       = this.models.hand;
    var rounds      = this.models.round;
    var players     = this.models.player;
    var answers     = this.models.answer;
    var a_cards     = this.models.a_card;
    var Promise     = this.Promise;
    var Sequelize   = this.models.Sequelize;
    var gameOptions = this.models.gameOptions;

    var cardsNeeded;

    var pGame = games.find({
        where: { id: game.id },
        include: [{
            model: players, include: [{ model: a_cards, as: "hand" }] }, {
            model: rounds,  include: [{ model: answers, include: [{model: a_cards, as: "cards" }] }]
        }]
    });
    var pGameOptions = gameOptions.find({ where: { lobbyId: game.lobbyId } } );

    return Promise.join(pGame, pGameOptions, function(pGame, pGameOptions){
    // trouver qui a besoin de combien de cartes
        if( ! pGame.players ){ throw new GameError("missing.players"); }
        if( ! pGameOptions  ){ throw new GameError("missing.gameOptions"); }
        cardsNeeded = { "total": 0 };
        pGame.players.forEach(function(player){
            cardsNeeded[player.id] = pGameOptions.hand_limit - player.hand.length;
            cardsNeeded.total     += pGameOptions.hand_limit - player.hand.length;
        });

    // si personne n'a besoin de cartes, sauter la pige
        if(cardsNeeded.total < 1){
            return Promise.all([null, null]);
        }

    // bâtir le deck en trouvant qui a quelle(s) carte(s)
        var played_ids = [];
        pGame.rounds.forEach(function(round){
            round.answers.forEach(function(answer){
                played_ids.push(answer.id);
            })
        });
        pGame.players.forEach(function (player) {
            player.hand.forEach(function (card) {
                played_ids.push(card.id);
            });
        });
        var params            = {};
            params.where      = ( played_ids.length > 0 ) ? { id: { $not: played_ids } } : {};
            params.order      = [ Sequelize.fn('RAND') ];
            params.attributes = [ "id" ];

    // finalement, le (restant du) deck
        return Promise.all([ pGame, a_cards.findAll(params) ]);
    })
    .spread(function(pGame, aCards) {
        var cardsDrawed = [];
        if( ! ( pGame == null || aCards == null ) ){
            var cardIds = aCards.slice(0, cardsNeeded.total).map(function(c){ return c.id });
            pGame.players.forEach(function(player){
                for(var i = 0; i < cardsNeeded[player.id]; i++){
                    cardsDrawed.push({ "playerId": player.id, "aCardId": cardIds.pop()});
                }
            });
        }
        return hands.bulkCreate(cardsDrawed);
    })
    .then(function(){
        return game;
    });
};

methods.setRoundWinner = function(answer) {
    var Promise = this.Promise;
    return answer.getRound()
    .then(function(round){
        return Promise.all([
            round.getAnswers(),
            round.getGame()
            .then(function(game){
                return game.getPlayers();
            })
        ]);
    })
    .spread(function(answers, players){
        // -1, le czar ne vote pas
        if(answers.length != players.length - 1) throw new GameError("missing.answers");
        return answers;
    })
    .then(function(answers){
        answers.forEach(function(ans){
            if(ans.winner) throw new GameError("duplicate.winner");
        });
        answer.winner = true;
        return answer.save();
    });
};

methods.playAnswer = function(round, player, cards){
    var hands = this.models.hand;
    var answers = this.models.answer;
    var Promise = this.Promise;

    var cardIds = [];
    cards.forEach(function(card){
            cardIds.push(card.id);
    });

    var validatePlayer = Promise.try(function(){
        if(round.czarId === player.id) throw new GameError("invalid.player");
        return true;
    });
    
    var validateSubmitted = round.getAnswers({where : { playerId: player.id } })
    .then(function(ans){
        if(ans.length > 0){
            throw new GameError("duplicate.answer"); }
        return (ans.length > 0);
    });

    var validateAnswerCount = round.getQuestion()
    .then(function(question){
        if(cards.length != question.a_count){
            throw new GameError("missing.card"); }
        return (cards.length != question.a_count);
    });

    return this.Promise.join(validatePlayer, validateSubmitted, validateAnswerCount, function(){
        return answers.create();
    })
    .then(function(answer){
        return Promise.all([
            answer.setPlayer(player),
            answer.setRound(round),
            answer.setCards(cards)
        ]);
    })
    .spread(function(answer){
        return Promise.all([answer, hands.destroy({where:{aCardId:{$in:cardIds}}})]);
    })
    .spread(function(answer){
        return answer;
    });
};

module.exports = GameEngine;
