var fs = require('fs'),
	path = require('path');

var exports = {};
var ext = ".json";
fs.readdirSync(__dirname)
  .filter(function(file) {
    return file.indexOf(ext) !== 0 && file !== 'index.js';
  })
  .forEach(function(file) {
    var name = path.basename(file, ext);
    var abspath = __dirname + "/" + file;
    exports[name] = require(abspath);
  });
module.exports = exports;