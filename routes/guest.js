module.exports = function(app) {
    var route = '/guest';
    var operations = app.settings.operations;

    app.post(route, function(req, res, next) {
        var username = req.body.username;
        
        operations.Auth.newGuest(username,{
            loggedIn: function(result){
                res.status(201).send(result);
            },
            error: function(err){
                next(err);
            }
        });
  });
}