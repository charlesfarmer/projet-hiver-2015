module.exports = function(app){
    var route = '/login';
    var operations = app.settings.operations;

    app.post(route, function(req, res, next){
        var username = req.body.username;
        var password = req.body.password;
        operations.Auth.login(username, password, {
            error: function(err){
                next(err);
            },
            loggedIn: function(token){
                res.send(token);
            }
        });
    });
}