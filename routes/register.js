module.exports = function(app){
    var route = '/register';
    var operations = app.settings.operations;

    app.post(route, function(req, res, next){
        var username = req.body.username;
        var password = req.body.password;
        
        operations.Members.register(username, password, {
            created: function(result){
                res.sendStatus(201);
            },
            error: function(err){
                next(err);
            }
        });
    });
}