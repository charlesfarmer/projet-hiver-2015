/**
 * Module dependencies.
 */

//Raccourcis pour pas faire ../../../../...... dans les require;
global.requireBase = require(__dirname+"/utils/RequireBase")(__dirname);
global.config = requireBase('config');

//Config de l'application
var colors = require('colors');
console.log("Config : ".yellow, config);

//Utilitaires
var util = require('util');
var requireFu = require('require-fu');

// Nos modules
var backend = requireBase("backend");
var middleware = requireBase('middleware');

// Modules d'express (HTTP) et middleware
var http = require('http');
var express = require('express');
var body_parser = require('body-parser');
var method_override = require('method-override');
var morgan = require('morgan');
var app = module.exports = express();

// Configuration de l'application express, middleware
app.set('operations', backend);
app.use(body_parser.json());
app.use(body_parser.urlencoded({ extended: true }));
app.use(method_override());
app.use(middleware.CrossOrigin(config.server.xOrigin));

//Config si en développement
if(app.settings.env == 'development'){
    app.use(middleware.ConnectionInfo());
    app.use(morgan('dev'));
}

//Routes HTTP
requireFu(__dirname+"/routes")(app);
app.use(middleware.ErrorHandling());

// Configuration Events pour le Socket
var server = http.createServer(app);
var io = require('socket.io').listen(server);
io.use(middleware.SocketJWT(config.server.jwt.secret));
io = require(__dirname + '/events')(app, io);

// Écoute...
server.listen(config.server.port, function(){
    console.log("Express server listening on port %d in %s mode".green, server.address().port, app.settings.env);
});