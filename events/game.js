module.exports = function(app, io){

    var operations = app.settings.operations;

    io.on("connection", function(socket){
        socket.on("start", function(lobbyID){
            var userId = socket.decoded_token.userId;
            var dataAvailable = ! ( lobbyID == null || userId == null );
            if(dataAvailable){
                operations.Game.newGame(userId, lobbyID,{
                    started: function(lobbyId, gameHash){
                        newRound(io, gameHash, lobbyId);
                    },
                    error: function(err){
                        console.log("ERROR STARTING GAME:", err.message);
                        io.to(socket.id).emit("error", err.message);
                    }
                });
            } else {
                io.to(socket.id).emit("error", "missing data to start game");
            }
        })
        .on("answer", function(data){
            var userId = socket.decoded_token.userId;
            var dataAvailable = ! ( data == null || data.roundId == null || data.playerId == null || data.cardIds == null );
            if(dataAvailable){
                operations.Game.playAnswer(userId, data.roundId, data.playerId, data.cardIds,{
                    answered:function(lobbyId, playerId){
                        io.to(lobbyId).emit("answered", playerId);
                    },
                    answers:function(lobbyId, answers){
                        io.to(lobbyId).emit("answers", answers);
                    },
                    error:function(err){
                        console.log("ERROR ANSWERING:", err.message);
                        io.to(socket.id).emit("error", err.message);
                    }
                });
            } else {
                io.to(socket.id).emit("error", "missing data to play answer");
            }
        })
        .on("vote", function(data){
            var userId = socket.decoded_token.userId;
            var dataAvailable = ! ( data == null || data.answerId == null );
            if(dataAvailable) {
                operations.Game.vote(userId, data.answerId,{
                    voted:function(lobbyId, answer){
                        io.to(lobbyId).emit("voted", answer);
                    },
                    gameOver:function(lobbyId, leaders){
                        io.to(lobbyId).emit("over", leaders);
                    },
                    newRound:function(lobbyId, gameHash){
                        newRound(io, gameHash, lobbyId);
                    },
                    error:function(err){
                        console.log("ERROR VOTING:", err.message);
                        io.to(socket.id).emit("error", err.message);
                    }
                });
            } else {
                io.to(socket.id).emit("error", "missing data to vote");
            }
        });
    });
    return io;
};

//Utilitaire pour envoyer les événemenent liés à new round
function newRound(io, gameHash, lobbyID){
    io.to(lobbyID).emit('started', gameHash.round);
    var crappyHash = {};
    for(var index in gameHash.players){
        var player = gameHash.players[index];
        crappyHash[player.userId] = {
            "playerId":player.id,
            "score":player.score
        }
        io.to(player.userId).emit('handed', player);
    }
    io.to(lobbyID).emit('mapped', crappyHash);
}