var fs = require('fs'),
    path = require('path');

var ext = '.js';

function err(io, socket, err){
    console.log(err.stack);
    io.to(socket.id).emit("error", err.message);
}
module.exports = function(app, io) {
    fs.readdirSync(__dirname)
        .filter(function(file) {
            return file.indexOf(ext) !== 0 && file !== 'index.js';
        })
        .forEach(function(file) {
            var abspath = __dirname + "/" + file;
            io = require(abspath)(app, io, err);
        });
    return io;
}