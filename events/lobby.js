module.exports = function(app, io, err){
    var operations = app.settings.operations;

    io.on('connection', function(socket){
        var userId = socket.decoded_token.userId;
        socket.join(userId);
        
        operations.Lobbies.match(userId,{
            joined:function(lobbyHash){
                socket.join(lobbyHash.id);
                socket.broadcast.to(lobbyHash.id).emit('joined', lobbyHash);
                io.to(userId).emit('join', lobbyHash);
                console.log(userId + ": room " + lobbyHash.id + "joined");
            },
            started:function(gameHash){
                io.to(gameHash.lobbyId).emit('started', gameHash.round);
                for(var player in gameHash.players){
                    io.to(gameHash.players[player].userId).emit('handed', gameHash.players[player]);
                }
            },
            error:function(err){
                console.log(userId + ": ERROR JOINING LOBBY:",err.message);
                console.log(err.stack);
                io.to(socket.id).emit('error', err.message);
            }
        });

        socket.on('disconnect', function(){
            userId = socket.decoded_token.userId;

            operations.Lobbies.leave(userId,{
                left:function(lobbyId){
                    //FIXME: Risque de peter, vérifier que socket.to = io.to apres la déconnection
                    io.to(lobbyId).emit('left', userId);
                },
                killed:function(lobbyId){
                    io.to(lobbyId).emit('killed');
                },
                error:function(err){
                    console.log(userId + ": ERROR LEAVING LOBBY:", err.message);
                    io.to(socket.id).emit('error', err.message);
                }
            });
        })
    });
    return io;
};
