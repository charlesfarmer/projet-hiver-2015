var clients = {};
var connectedSockets = {};

module.exports = function(app, io){
    io.on('connection', function(socket){
        address = socket.handshake.address;
        socket.on('set username', function(username){
            if( ! ( username in clients ) ){
                console.log('Greeting ' + username + ' at ' + clients[username]);
                clients[username] = socket.id;
                connectedSockets[socket.id] = username;
                io.to(socket.id).emit('greet', username);
            }
            else {
                console.log('Username ' + username + ' already in use');
                io.to(socket.id).emit('error', { 'usernameInUse' : true, 'username' : username });
            }
        })
        .on('chat message', function(message){
            if(socket.rooms.length > 1) {
                console.log('>>[GAME MESSAGE] ' + message);
                io.to("partie").emit('chat message', '[' + ( connectedSockets[socket.id] ? connectedSockets[socket.id] : address ) + '][GAME MESSAGE] ' + message);
            } else {
                console.log('>> ' + message);
                io.emit('chat message', '[' + ( connectedSockets[socket.id] ? connectedSockets[socket.id] : address ) + '] ' + message);
            }
        });
    });
    return io;
}
