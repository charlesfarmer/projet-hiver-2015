var util = require("util");
var Operation = require("./operation");
var OperationError = Operation.Error;

function Auth(params){
    Auth.super_.call(this, params);
    this.className = "Auth";
}

util.inherits(Auth, Operation);
var methods = Auth.prototype;

methods.newGuest = function(name, callbacks){
    if(!name || name === "") throw new OperationError("missing.username");
    var authService = this.services.Auth;
    this.transaction(function(t){
        return authService.newGuest(name);
    })
    .then(function(hash){
        callbacks.loggedIn(hash);
    })
    .catch(function(err){
        callbacks.error(err);
    });
}

methods.login = function(username, password, callbacks){
    var service = this.services.Auth;
    if(!username || username === "") throw new OperationError("missing.username");
    if(!password || password === "") throw new OperationError("missing.password");
    this.transaction(function(t){
        return service.login(username,password);
    })
    .then(function(hash){
        callbacks.loggedIn(hash);
    })
    .catch(function(err){
        callbacks.error(err);
    });
}

module.exports = Auth;