var _ = require('lodash');
var util = require("util");
var Operation = require("./operation");
var OperationError = Operation.Error;

function Lobbies(params){
    Lobbies.super_.call(this, params);
    this.className = "Lobbies";
}
util.inherits(Lobbies, Operation);
var methods = Lobbies.prototype;

// Connections
methods.match   = function (userID, callbacks){
    var services = this.services;
    var joinedLobby;
    var newGame;
    this.transaction(function(t){
        return services.Lobbies.match(userID)
        .then(function(lobbyHash){
            joinedLobby = lobbyHash;
            if(lobbyHash.users.length >= lobbyHash.size){
                return services.Game.newGame(lobbyHash.id)
                .then(function(game) {
                    return services.Game.newRound(game.id);
                })
                .then(function(game){
                    newGame = game;
                })
            }
        })
    })
    .then(function(){
        if(joinedLobby) callbacks.joined(joinedLobby);
        if(newGame) callbacks.started(newGame);
    })
    .catch(function(err){
        callbacks.error(err);
    });
};

methods.leave   = function (userID, callbacks){
    var service = this.services.Lobbies;
    this.sequelize.transaction(function (t) {
        return service.leave(userID);
    }).spread(function(lobbyId, isKilled) {
        callbacks.left(lobbyId);
        if(isKilled)callbacks.killed();
    }).catch(function (err) {
        callbacks.error(err);
    });
};

/* Pas utilisé pour l'instant

methods.join    = function (lobbyID, callbacks){
    var service = this.sevices.Lobbies;
    this.transaction(function (t) {
        return service.join(lobbyID,userID);
    }).then(function(lobbyHash){
        return lobbyHash;
    }).catch(function (err) {
        throw err;
    });
};



//CRUD
methods.create  = function (options, callbacks){
    var service = this.sevices.Lobbies;
    this.transaction(function (t) {
        return service.create(options);
    }).then(function (result) {
        return result;
    }).catch(function (err) {
        throw err;
    });
};

methods.close   = function (lobbyID, callbacks){
    var service = this.sevices.Lobbies;
    this.transaction(function(t){
        return service.close(lobbyID);
    })
    .then(function(result){
        return result;
    })
    .catch(function(err){
        throw err;
    });
};

methods.update  = function (lobbyID, options, callbacks){
    var service = this.sevices.Lobbies;
    this.transaction(function(t){
        return service.update(lobbyID,options);
    })
    .then(function(result){
        return result;
    })
    .catch(function(err){
        throw err;
    });
};

methods.get     = function (lobbyID, callbacks){
    var service = this.sevices.Lobbies;
    this.transaction(function(t){
        service.get(lobbyID);
    })
    .then(function(result){
        return result;
    })
    .catch(function(err){
        throw err;
    });
};

methods.getOpen = function (callbacks){
    var service = this.sevices.Lobbies;
    this.transaction(function(t){
        return service.getOpen();
    })
    .then(function(lobbies){
        return lobbies;
    })
    .catch(function(err){
        throw err;
    });
};
*/
module.exports  = Lobbies;
