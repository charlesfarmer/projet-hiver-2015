var fs = require('fs');
var path = require('path');

module.exports = function(models, services) {
    var exports = {};

    fs.readdirSync(__dirname)
        .filter(function(file) {
            return file !== 'operation.js' && file !== 'index.js';
        })
        .forEach(function(file) {
            var abspath = __dirname + "/" + file;
            var modelClass = require(abspath);
            var params = {
                models:models,
                config:config,
                services:services
            };
            var instance = new modelClass(params);
            exports[instance.className] = instance;
        });

    return exports;
}