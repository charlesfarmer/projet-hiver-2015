var util = require("util");
var Operation = require("./operation");
var OperationError = Operation.Error;
var lodash = require("lodash");

function Game(params){
    Game.super_.call(this, params);
    this.className = "Game";
};
util.inherits(Game, Operation);
var methods = Game.prototype;

//Nettoie un gamehash des objets de sequelize etc
function formatGameHash(game){
    return game.getScores()
    .then(function(scores){
        var goodHash = {"players":[]};
        //Nettoyage du player
        for(var i = 0; i < game.players.length; i++){
            var player = game.players[i];
            player.score = scores[i].score;
            //Nettoyage de la main
            for(var j = 0; j < player.hand.length; j++){
                player.hand[j] = lodash.pick(player.hand[j], "id", "content");
            }
            goodHash.players[i] = lodash.pick(player, "id", "userId", "score", "hand");
        }
        //Nettoyage du round
        var round = game.rounds.pop();
        //Nettoyage de la q_card
        round.question = lodash.pick(round.question, "content", "a_count");
        goodHash.round = lodash.pick(round, "id", "czarId", "question");
        return goodHash;
    });
}

methods.newGame = function(userID, lobbyID, callbacks){
    var self = this;
    var services = this.services;
    return this.transaction(function(){
        return services.Game
        .newGame(userID, lobbyID)
        .then(function(game){
            return services.Game.newRound(game.id);
        })
        .then(function(game){
            return formatGameHash(game);
        });
    })
    .then(function(gameHash){
        callbacks.started(lobbyID,gameHash);
    })
    .catch(function(err){
        callbacks.error(err);
    });
};

methods.vote = function(userID, answerID, callbacks) {
    var self = this;
    var services = this.services;
    var lobbyId;
    var pickedAnswer;
    var winner;
    var newRound;
    return this.transaction(function(){
        return services.Game.vote(userID, answerID)
        .spread(function(answer, game){
            pickedAnswer = lodash.pick(answer, 'id', 'playerId');
            lobbyId = game.lobbyId;
            return game;
        })
        .then(function(game){
            return game.isFinished()
            .then(function(isFinished){
                if(isFinished){
                    return game.getLeaders()
                    .then(function(leaders){
                        winner = leaders;
                    });
                } else {
                    return services.Game.newRound(game.id)
                    .then(function(game){
                        return formatGameHash(game);
                    })
                    .then(function(gameHash){
                        newRound = gameHash;
                    });
                }
            });
        });
    })
    .then(function(){
        if(pickedAnswer) callbacks.voted(lobbyId, pickedAnswer);
        if(winner) callbacks.gameOver(lobbyId, winner);
        if(newRound) callbacks.newRound(lobbyId, newRound);
    })
    .catch(function(err){
        callbacks.error(err);
    });
};

methods.playAnswer = function(userID, roundID, playerID, cardIDs, callbacks){
    var services = this.services;
    var lobbyId;
    return this.transaction(function(){
        return services.Game.playAnswer(userID,roundID,playerID,cardIDs);
    })
    .spread(function(game, round, answer){
        callbacks.answered(game.lobbyId, answer.playerId);
        if(round.answers.length >= game.players.length - 1){
            var answersArray = [];
            round.answers.forEach(function(answer){
                var contents = [];
                answer.cards.forEach(function(card){
                    contents.push(card.content);
                });
                answersArray.push({ id: answer.id, contents: contents });
            });
            callbacks.answers(game.lobbyId, answersArray);       
        }
    })
    .catch(function(err){
        callbacks.error(err);
    });
};

module.exports = Game;