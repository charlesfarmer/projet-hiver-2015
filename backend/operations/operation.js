var methods = Operation.prototype;
var ChainableError = requireBase("utils/ChainableError");
var util = require('util');

function Operation(params){
    this.className = "Operation";
    this.services = params.services;
    this.models = params.models;
    
    this.Sequelize = this.models.Sequelize;
    this.sequelize = this.models.sequelize;
    this.Promise = this.models.Sequelize.Promise;
}

//Wrapper cute pour les options
methods.transaction = function(callback){
    options = {autocommit:false};
    return this.sequelize.transaction(options, callback);
}

// Erreur Operation
function OperationError(message) {
    OperationError.super_.call(this, message);
    this.name = "OperationError";
}
util.inherits(OperationError, ChainableError);
Operation.Error = OperationError;

module.exports = Operation;