var util = require("util");
var Operation = require("./operation");
var OperationError = Operation.Error;

function Members(params){
    Members.super_.call(this, params);
    this.className = "Members";
}

util.inherits(Members, Operation);
var methods = Members.prototype;

methods.register = function(username, password, callbacks){
    var memberService = this.services.Members;
    if(!username || username === "") throw new OperationError("missing.username");
    if(!password || password === "") throw new OperationError("missing.password");
    this.transaction(function(t){
        return memberService.register(username, password);
    })
    .then(function(hash){
        callbacks.created(hash);
    })
    .catch(function(err){
        callbacks.error(err);
    });
}

module.exports = Members;