var models = require(__dirname+'/models');
var services = require(__dirname +'/services')(models);
var operations = require(__dirname+'/operations')(models,services);

module.exports = operations;