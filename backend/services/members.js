var util = require("util");
var Service = require("./service");
var ServiceError = Service.Error;
var passwordHash = require('password-hash');

function Members(params){
    Members.super_.call(this, params);
    this.className = "Members";
}

util.inherits(Members, Service);
var methods = Members.prototype;

methods.register = function(username, password){
    var service = this;
    var members = this.models.member;
    if(!username || username === "") throw new ServiceError("missing.username");
    if(!password || password === "") throw new ServiceError("missing.password");
    return members.all({where:{username:username}})
    .then(function(members){
        if(members.length > 0) throw new ServiceError("duplicate.username");
    })
    .then(function(){
        var hash = passwordHash.generate(password);
        return members.create({username:username, password:hash});
    })
    .then(function(member){
        return {id:member.id};
    })
    .catch(function(err){
        throw new ServiceError(err);
    });
}

methods.getProfile = function(memberID){

}

methods.getAll = function(){
    return this.models.user.findAll()
    .then(function(results){
        var values = JSON.stringify(results);
        return values;
    });
}

module.exports = Members;