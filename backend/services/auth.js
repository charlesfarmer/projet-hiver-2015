var util = require("util");
var Service = require("./service");
var ServiceError = Service.Error;
var passwordHash = require('password-hash');
var jwt = require('jsonwebtoken');

function Auth(params){
    Auth.super_.call(this, params);
    this.className = "Auth";
}

util.inherits(Auth, Service);
var methods = Auth.prototype;

methods.newGuest = function(name){
    var users = this.models.user;
    var members = this.models.member;
    var service = this;
    if(!name || name === "") throw new ServiceError("missing.username");
    return members.all({where:{username:name}})
    .then(function(members){
        if(members.length > 0) throw new ServiceError("duplicate.username");
    })
    .then(function(){
        return users.create({"name":name});
    })
    .then(function(user){
        return service.setToken(user);
    })
    .then(function(user){
        return {token: user.token};
    })
    .catch(function(err){
        throw err;
    });
}

methods.login = function(username, password){
    var members = this.models.member;
    var users = this.models.user;
    var service = this;
    if(!username || username === "") throw new ServiceError("missing.username");
    if(!password || password === "") throw new ServiceError("missing.password");
    return members.find({where:{username:username}})
    .then(function(member){
        if(member == null) throw new ServiceError("invalid.username");
        return member;
    })
    .then(function(member){
        var validPW = passwordHash.verify(password, member.password);
        if(!validPW) throw new ServiceError("invalid.password");
        return member;
    })
    .then(function(member){
        return users.create({"name":member.username, memberId:member.id});
    })
    .then(function(user){
        return service.setToken(user);
    })
    .then(function(user){
        return {token: user.token};
    })
    .catch(function(err){
        throw err;
    });
}

methods.setToken = function(user){
    var secret = config.server.jwt.secret;
    var payload = {
        userId: user.id,
        timestamp: Date.now()
    };
    var token = jwt.sign(payload, secret);
    user.token = token;
    return user.save().catch(function(err){
        throw err;
    });
}

module.exports = Auth;