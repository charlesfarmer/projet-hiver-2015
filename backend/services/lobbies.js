var _ = require('lodash');
var util = require("util");
var Service = require("./service");
var ServiceError = Service.Error;

function Lobbies(params){
    Lobbies.super_.call(this, params);
    this.className = "Lobbies";
}
util.inherits(Lobbies, Service);
var methods = Lobbies.prototype;

// Connections
methods.match   = function (userID){
    var service = this;
    var options = config.gameOptions;
    var Promise = this.Promise;
    //TODO: ajouter de l'intelligence à la recherche de lobbies
    return service.getOpen().then(function(open_lobbies){
        if( open_lobbies.length > 0 ) {
            return open_lobbies[Math.floor(Math.random()*open_lobbies.length)].id;
        } else {
            return service.create(options).then(function(lobby){
                return lobby.id;
            });
        }
    })
    .then(function(lobbyID){
        if(lobbyID == null){
            throw new ServiceError("missing.lobby") }
        if(userID == null){
            throw new ServiceError("missing.user") }
        return service.join(lobbyID, userID);
    })
    .then(function(lobbyHash){
        return lobbyHash;
    })
    .catch(function(err){
        throw err;
    });
};

methods.join    = function (lobbyID, userID){
    var users = this.models.user;
    var decks = this.models.deck;
    var lobbies = this.models.lobby;
    var gameOptions = this.models.gameOptions;
    var Promise = this.Promise;

    var lobbyHash = {};
    var lobbyCriteria = {};
        lobbyCriteria.where = { id: lobbyID };
        lobbyCriteria.include = [
            {model: users},
            {model: gameOptions, include: [decks]}
        ];
    var lobby = lobbies.find(lobbyCriteria);
    var user = users.find(userID);
    return Promise.join(lobby, user, function(lobby, user){
        if(lobby == null){
            throw new ServiceError("missing.lobby"); }
        if(user == null){
            throw new ServiceError("missing.user"); }
        if(lobby.isFull()){
            throw new ServiceError("Lobby is full"); }
        return Promise.all([lobby, lobby.addUser(user)]);
    }).spread(function(lobby) {
        return lobby.reload();
    }).then(function(lobby){
        lobbyHash = {
            id: lobby.id,
            name: lobby.name,
            size: lobby.size,
            users: []
        };
        // remplir les joueurs
        lobby.users.forEach(function (user) {
            lobbyHash.users.push(_.pick(user, ['id','name']));
        });
        // remplir les options
        lobbyHash.gameOption = _.pick(lobby.gameOption, ['player_min', 'hand_limit', 'point_limit', 'player_min']);
        lobbyHash.gameOption.deck = lobby.gameOption.deck.name;
        return lobbyHash;
    }).catch(function (err) {
        throw err;
    });
};

methods.leave   = function (userID){
    var users = this.models.user;
    var lobbies = this.models.lobby;
    var options = this.models.gameOptions;
    var players = this.models.player;
    var Promise = this.Promise;

    return users.find(userID).then(function(user){
        if(user == null){
            throw new ServiceError("missing.user"); }
        return Promise.all([user.getLobby({include:[users,options]}), user]);
    })
    .spread(function(lobby,user){
        if(lobby == null){
            throw new ServiceError("missing.lobby"); }
        return Promise.all([lobby, lobby.getGame({include:[players]}), user.destroy()]);
    })
    .spread(function(lobby, game){
        var isKill = false;
        var gameDeleted = null;
        if(game !== null){
            isKill = (lobby.gameOption.player_min > game.players.length);
            if(isKill){
                gameDeleted = game.destroy(); }
        }
        return Promise.all([lobby.id, isKill, gameDeleted]);
    }).catch(function (err) {
        throw err;
    });
};

//CRUD
methods.create  = function (options){
    var Promise = this.Promise;
    var lobbies = this.models.lobby;
    var gameOptions = this.models.gameOptions;
    return Promise.all([
        gameOptions.create(options.game),
        lobbies.create(options.lobby)
    ])
    .spread(function(gameOpts, lobby){
        return lobby.setGameOption(gameOpts);
    }).then(function (result) {
        return {id: result.id};
    }).catch(function (err) {
        throw err;
    });
};

methods.close   = function (lobbyID){
    var lobbies = this.models.lobby;
    var options = this.models.gameOptions;
    var users = this.models.user;
    var games = this.models.game;

    return lobbies.find(lobbyID).then(function(lobby){
        if(lobby == null) {
            throw new ServiceError("missing.lobby");
        }
        return lobby.destroy();
    })
    .then(function(result){
        return {id: result.id};
    })
    .catch(function(err){
        throw err;
    });
};

methods.update  = function (lobbyID, options){
    var lobbies = this.models.lobby;
    var gameOptions = this.models.gameOptions;
    return lobbies.find(lobbyID).then(function(lobby){
        if(lobby == null) {
            throw new ServiceError("missing.lobby");
        }
        lobby.getGameOption().then(function(gameOption){
            if(gameOption !== null) {
                gameOption.updateAttributes(options.game);
            }
        });
        return lobby.updateAttributes(options.lobby);
    })
    .then(function(result){
        return {id: result.id};
    })
    .catch(function(err){
        throw err;
    });
};

methods.get     = function (lobbyID){
    var lobbies = this.models.lobby;
    return lobbies.find(lobbyID).then(function(lobby){
        if(lobby == null) {
            throw new ServiceError("missing.lobby");
        }
    })
    .then(function(result){
        return result.get({plain:true});
    })
    .catch(function(err){
        throw err;
    });
};

methods.getOpen = function (){
    var users = this.models.user;
    var lobbies = this.models.lobby;
    var games = this.models.game;
    var Promise = this.Promise;
    var sequelize = this.sequelize;
    return lobbies.findAll({ include: [users, games] }).then(function(lobbies){
        return lobbies.filter(function(lobby) {
            return ! ( lobby.hasGame() ||  lobby.isFull() );
        });
    })
    .then(function(lobbies){
        return lobbies;
    })
    .catch(function(err){
        throw err;
    });
};

module.exports  = Lobbies;
