var methods = Service.prototype;
var ChainableError = requireBase("utils/ChainableError");
var util = require("util");

function Service(params){
    this.className = "Service";
    this.models = params.models;
    this.Promise = this.models.Sequelize.Promise;
    this.sequelize = this.models.sequelize;
}
// Erreur de service
//ServiceError(inner,message) ou ServiceError(key,message)
function ServiceError(message) {
  ServiceError.super_.call(this, message);
  this.name = "ServiceError";
}
util.inherits(ServiceError, ChainableError);

//Wrapper cute pour les options
methods.transaction = util.deprecate(function(callback){
    options = {autocommit:false};
    return this.sequelize.transaction(options, callback);
}, "Service.transaction is deprecated: Transactions should be handled in operations");


Service.Error = ServiceError;

module.exports = Service;