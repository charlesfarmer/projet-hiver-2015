var util = require("util");
var Service = require("./service");
var ServiceError = Service.Error;
var GameEngine = requireBase("game/engine");
var lodash = require("lodash");

function Game(params){
    Game.super_.call(this, params);
    this.className = "Game";
    this.engine = new GameEngine(this.models);
};
util.inherits(Game, Service);
var methods = Game.prototype;

methods.newGame = function(userID, lobbyID){
    var games = this.models.game;
    var users = this.models.user;
    var lobbies = this.models.lobby;
    var engine = this.engine;
    var Promise = this.Promise;

    var user = users.find({ where: { lobbyId: lobbyID, id: userID }});
    var lobby = lobbies.find({ where: { id:lobbyID }});
    return Promise.join(user, lobby, function(user, lobby){
        if(user == null){
            throw new ServiceError("missing.player"); }
        if(lobby == null){
            throw new ServiceError("missing.lobby"); }
        return engine.create(lobby);
    })
    .then(function(game){
        return engine.initialize(game);
    })
    .catch(function(err){
        throw err;
    });
};

methods.newRound = function(gameID){
    var engine = this.engine;
    var models = this.models;
    var games = this.models.game;
    var q_cards = this.models.q_card;
    var a_cards = this.models.a_card;
    var answers = this.models.answer;
    var rounds = this.models.round;
    var players = this.models.player;
    var Promise = this.Promise;

    return games.find({ where: { id: gameID }})
    .then(function(game){
        if(game == null){
            throw new ServiceError("missing.game"); }
        return Promise.all([game, game.isFinished()]);
    })
    .spread(function(game, gameIsFinished){
        if(gameIsFinished){
            game.destroy().then(function(){
                throw new ServiceError("game.over"); });
        }
        return engine.fillHands(game);
    })
    .then(function(game){
        return engine.addRound(game);
    })
    .then(function(round){
        return games.find({
            where: { id: round.gameId },
            include: [{
                model: players, include: [{ model: a_cards, as: "hand", attributes: ['id','content'] }], attributes: ['id','userId'] }, {
                model: rounds,  include: [
                    { model: q_cards, as: "question", attributes: ['id','content','a_count'] },
                    { model: answers, include: [{model: a_cards, as: "cards", attributes: ['id', 'content'] }] }
                ],
                attributes: ['id', 'czarId', 'gameId']
            }],
            attributes: ['id','lobbyId']
        });
    })
    .catch(function(err){
        throw err;
    });
};

methods.vote = function(userID, answerID) {
    var engine = this.engine;
    var games = this.models.game;
    var rounds = this.models.round;
    var players = this.models.player;
    var answers = this.models.answer;
    var Promise = this.Promise;
    
    return answers.find( { where: { id: answerID } } )
    .then(function(answer){
        if ( answer == null ){
            throw new ServiceError("missing.answer"); }
        return Promise.all([ answer, answer.getRound() ]);
    })
    .spread(function(answer, round){
        var player = players.find({where:{gameId:round.gameId,userId:userID}});
        return Promise.all([answer, round, player]);
    })
    .spread(function(answer, round, player){
        if(player.id !== round.czarId){
                throw new ServiceError("missing.czar"); }
        return Promise.all([engine.setRoundWinner(answer), round.getGame()]);
    })
    .spread(function(answer, game){
        return Promise.all([lodash.pick(answer, ["id", "playerId"]), game]);
    })
    .catch(function(err){
        throw err;
    });
};

methods.playAnswer = function(userID, roundID, playerID, cardIDs){
    var engine = this.engine;
    var games = this.models.game;
    var rounds = this.models.round;
    var a_cards = this.models.a_card;
    var players = this.models.player;
    var answers = this.models.answer;
    var Promise = this.Promise;

    var round = rounds.find({
        where:{ id: roundID },
        include:[
            { model: games },
            { model: players, as: "czar" },
            { model: answers, include: [{ model: a_cards, as: "cards", attributes: ['content'] }] }
        ]
    })
    .then(function(round){
        if(round == null) throw new ServiceError("missing.round");
        return round;
    });
    var player = players.find({where:{id:playerID, userId:userID }})
    .then(function(player){
        if(player == null) throw new ServiceError("missing.player");
        return player;
    });
    var cards = player.then(function(player){
        return player.getHand({where: { id : { $in : cardIDs } } });
    })
    .then(function(cards){
        if(cards.length != cardIDs.length) throw new ServiceError("missing.card");
        return cards;
    });

    return Promise.join(round, player, cards, function(round, player, cards){
        var answer = engine.playAnswer(round,player,cards);
        var game = games.find({where:{id: round.gameId}, include: [players]});
        return Promise.all([game, round, answer]);
    })
    .spread(function(game, round, answer){
        return Promise.all([game.reload(), round.reload(), answer]);
    })
    .catch(function(err){
        throw err;
    });
};

methods.getAll = function(){
    return this.models.game.findAll()
    .then(function(results){
        var values = JSON.stringify(results);
        return values;
    });
};

module.exports = Game;
