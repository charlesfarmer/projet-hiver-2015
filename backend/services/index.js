var fs = require('fs');
var path = require('path');

module.exports = function(models) {
    var exports = {};

    fs.readdirSync(__dirname)
        .filter(function(file) {
            return file !== 'service.js' && file !== 'index.js';
        })
        .forEach(function(file) {
            var abspath = __dirname + "/" + file;
            var serviceClass = require(abspath);
            var params = {
                models:models,
                config:config
            };
            var instance = new serviceClass(params);
            exports[instance.className] = instance;
        });

    return exports;
}