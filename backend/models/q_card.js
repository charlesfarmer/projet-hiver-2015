module.exports = function(sequelize, DataTypes) {
    var modelName = "q_card";
    var attributes = {
        content: DataTypes.STRING,
        a_count: DataTypes.INTEGER
    };
    var settings = {
        paranoid: true,
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.deck);
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};