module.exports = function(sequelize, DataTypes) {
    var modelName = "player";
    var attributes = {
    };
    var settings = {
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.user, {onDelete:'cascade'});
                this.belongsTo(models.game);
                this.hasMany(models.answer);
                this.belongsToMany(models.a_card, {through: models.hand, as: "hand"});
            }
        },
        instanceMethods: {
            getPoints: function(){
                return this.getAnswers({where: {winner:true} })
                .then(function(wins){
                    return wins == null ? 0 : wins.length;
                });
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};