var _ = require("lodash");
module.exports = function(sequelize, DataTypes) {
    var modelName = "game";
    var attributes = {
    };
    var settings = {
        classMethods: {
            associate: function(models) {
                this.hasMany(models.round, {onDelete:'cascade'});
                this.hasMany(models.player, {onDelete:'cascade'});
                this.belongsTo(models.lobby);
            }
        },
        instanceMethods: {
            isStarted: function() { return this.rounds.length > 0; },
            isFinished: function(models) {
                return sequelize.Promise.all([
                    this.getRounds(),
                    this.getPlayers(),
                    this.getOptions()
                ]).spread(function(rounds, players, options){
                    var maxRoundsReached = rounds.length >= options.round_limit;
                    var futurePoints = [];
                    players.forEach(function(player){
                        futurePoints.push(player.getPoints());
                    });
                    return sequelize.Promise.all([futurePoints, maxRoundsReached])
                })
                .spread(function(scores, maxRoundsReached){
                    var playerWon = _.filter(scores, function(score){
                        return score >= options.point_limit;
                    }).length > 0;
                    return playerWon || maxRoundsReached;
                });
            },
            getScores: function(){
                var IDs = [];
                return this.getPlayers()
                .then(function(players){
                    var points = [];
                    players.forEach(function(player){
                        IDs.push(player.id);
                        points.push(player.getPoints());
                    });
                    return sequelize.Promise.all(points);
                })
                .then(function(scores){
                    var map = [];
                    for(var i = 0; i < IDs.length; i++){
                        var hash = {"playerId": IDs[i], "score":scores[i]};
                        map.push(hash);
                    }
                    return map;
                })
            },
            getOptions: function(){
                return this.getLobby().then(function(lobby){
                    return lobby.getGameOption();
                });
            },
            getLeaders: function(){
                return this.getScores()
                .then(function(scores){
                    var max = _.max(scores, "score").score;
                    var leaders = _.filter(scores, 'score', max);
                    var leaderIDs = _.pluck(leaders, "playerId");
                    return leaderIDs;
                });
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};