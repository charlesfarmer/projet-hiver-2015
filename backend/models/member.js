module.exports = function(sequelize, DataTypes) {
    var modelName = "member";
    var attributes = {
      username: {type: DataTypes.STRING, unique:true},
      password: DataTypes.STRING,
      avatar: DataTypes.BLOB
    };
    var settings = {
        paranoid: true,
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.locale);
                this.hasMany(models.user);
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};