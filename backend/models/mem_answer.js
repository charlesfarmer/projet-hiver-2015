module.exports = function(sequelize, DataTypes) {
    var modelName = "answer";
    var attributes = {
        winner: DataTypes.BOOLEAN
    }
    var settings = {
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.player, {onDelete:'cascade'});
                this.belongsTo(models.round, {onDelete:'cascade'});
                this.belongsToMany(models.a_card, {through: models.answer_card, as:"cards", onDelete:'cascade'});
            }
        }
    }
    return sequelize.define(modelName, attributes, settings);
};