module.exports = function(sequelize, DataTypes) {
    var modelName = "round";
    var attributes = {
    };
    var settings = {
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.game);
                this.belongsTo(models.q_card, {as: 'question'});
                this.belongsTo(models.player, {as: 'czar'});
                this.hasMany(models.answer, {onDelete:'cascade'});
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};