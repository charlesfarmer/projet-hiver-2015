module.exports = function(sequelize, DataTypes) {
    var modelName = "deck_options";
    var attributes = {
    }
    var settings = {
        engine: "MEMORY",
        tableName: 'mem_deck_options',
        classMethods: {
            associate: function(models) {
            }
        }
    }
    return sequelize.define(modelName, attributes, settings);
};