module.exports = function(sequelize, DataTypes) {
    var modelName = "lobby";
    var attributes = {
        name: DataTypes.STRING,
        size: DataTypes.INTEGER
    };
    var settings = {
        classMethods: {
            associate: function(models) {
                this.hasMany(models.user);
                this.hasOne(models.game, {onDelete:'cascade'});
                this.hasOne(models.gameOptions, {onDelete:'cascade'});
                this.belongsTo(models.locale);
            }
        },
        instanceMethods: {
            isFull: function() {
                return this.users.length >= this.size;
            },
            hasGame: function() {
                return this.game !== null;
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};