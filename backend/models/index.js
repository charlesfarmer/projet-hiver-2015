var fs = require('fs'),
    path      = require('path'),
    Sequelize = require('sequelize'),
    CLS       = require('continuation-local-storage');

var namespace = CLS.createNamespace('CAH-sequelize');
Sequelize.cls = namespace;

var sequelizeOpts = {};
sequelizeOpts.logging = config.database.logging;
var sequelize = new Sequelize(config.database.name, config.database.username, config.database.password, sequelizeOpts);
var db        = {};

//Import récursif des models
function importModels(directory){
  fs.readdirSync(directory)
    .forEach(function(file) {
      var absPath = path.join(directory, file);
      var stats = fs.statSync(absPath);
      if(stats.isDirectory()){
        importModels(absPath);
      }
      else if(file !== 'index.js'){
        var model = sequelize.import(absPath);
        db[model.name] = model;
      }
    });
}
importModels(__dirname);

//Créer les associations
Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

//Recréer la BD
if(config.database.sync){
  sequelize.query('SET FOREIGN_KEY_CHECKS = 0')
  .then(function() {
      return sequelize.sync({
              force: config.database.forceSync
            });
  })
  .then(function() {
    console.log('Database synchronised')
    return sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  })
  .then(function(){
    if(config.database.forceSync){
        console.log('Loading data...');
        //Charge les données
        var lastPromise = null;
        var dataDir = path.resolve("backend/data");
        console.log(dataDir);
        fs.readdirSync(dataDir).forEach(function(file){
          console.log("Got: "+file);
          var modelName = path.basename(file, ".json");
          var filePath = path.join(dataDir, file);
          var jsonData = require(filePath);
          jsonData.forEach(function(table){
              var promise = db[table.model].bulkCreate(table.data);
              if(lastPromise != null){
                lastPromise.then(function(){
                  return promise;
                });
              }
              lastPromise = promise;
          });
        });
        return lastPromise;
    }
  })
  .then(function(){
    if(config.database.forceSync) console.log('...done loading data.');
  })
  .catch(function(ee) {
      console.log(ee);
  });
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;