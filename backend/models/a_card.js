module.exports = function(sequelize, DataTypes) {
    var modelName = "a_card";
    var attributes = {
        content: DataTypes.STRING
    };
    var settings = {
        paranoid: true,
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.deck);
                this.belongsToMany(models.answer, {through: models.answer_card});
                this.belongsToMany(models.player, {through: models.hand});
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};