module.exports = function(sequelize, DataTypes) {
    var modelName = "user";
    var attributes = {
        name: DataTypes.STRING,
        token: DataTypes.STRING
    };
    var settings = {
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.member);
                this.belongsTo(models.lobby);
                this.hasMany(models.player);
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};