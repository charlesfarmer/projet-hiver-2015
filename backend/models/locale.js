module.exports = function(sequelize, DataTypes) {
    var modelName = "locale";
    var attributes = {
        code: DataTypes.STRING
    };
    var settings = {
        paranoid: true,
        classMethods: {
            associate: function(models) {
                this.hasMany(models.member);
                this.hasMany(models.deck);
                this.hasMany(models.lobby);
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};