module.exports = function(sequelize, DataTypes) {
    var modelName = "deck";
    var attributes = {
        name: DataTypes.STRING
    }
    var settings = {
        paranoid: true,
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.locale);
                this.hasMany(models.gameOptions);
                this.hasMany(models.q_card);
                this.hasMany(models.a_card);
            }
        }
    }
    return sequelize.define(modelName, attributes, settings);
};