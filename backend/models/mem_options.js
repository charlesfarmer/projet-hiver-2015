module.exports = function(sequelize, DataTypes) {
    var modelName = "gameOptions";
    var attributes = {
        round_limit:DataTypes.INTEGER,
        point_limit:DataTypes.INTEGER,
        hand_limit:DataTypes.INTEGER,
        player_min:DataTypes.INTEGER
    };
    var settings = {
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.deck);
                this.belongsTo(models.lobby);
            }
        }
    };
    return sequelize.define(modelName, attributes, settings);
};