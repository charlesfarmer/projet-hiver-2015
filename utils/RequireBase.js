module.exports = function(base){
    function requireBase(modulePath){
        return require(base+"/"+modulePath);
    }
    
    return requireBase;
}
