var util = require("util");

function ChainableError(innerError){
    if(innerError instanceof ChainableError){
        this.innerError = innerError;
        this.message = innerError.message
    }
    else if(typeof innerError == 'string'){
        this.message = innerError;
    }
    ChainableError.super_.call(this);
    ChainableError.super_.captureStackTrace(this, arguments.callee);
}
ChainableError.prototype.toString = function() {
    var msg;
    if(this.innerError){
        var format = "[ %s ] InnerError: %s";
        msg = util.format(format, this.name, this.innerError.toString());
    }
    else{
        var format = "[ %s ] %s";
        var string = ChainableError.super_.toString();
        msg = util.format(format, this.name, string);
    }
    return ;
};

util.inherits(ChainableError, Error);

module.exports = ChainableError;