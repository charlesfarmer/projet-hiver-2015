module.exports = function(){
    return function(err,req,res,next){
        if(err.message){
            res.status(400).send(err.message);
        }
        else{
            console.log(err);
            res.sendStatus(500);
        }
        next(err);
    }
}