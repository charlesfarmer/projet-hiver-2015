module.exports = function(settings){
    return function (req, res, next){
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', settings.url);
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', settings.methods);
        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', settings.headers);
        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', settings.credentials);
        // Pass to next layer of middleware
        next();
    }
}