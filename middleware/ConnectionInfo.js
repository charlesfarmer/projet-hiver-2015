var util = require('util');
module.exports = function(){
    return function(req, res, next){
        var message = "Connection from %s".white;
        var ip = req.ip.cyan
        var output = util.format(message, ip);
        console.log(output);
        next();
    }
}