var jwt = require('jsonwebtoken');
module.exports = function(secret){
    return function(req, res, next){
        var token = req.headers["x-access-token"];
        if (typeof token !== 'undefined') {
            var user = jwt.decode(token, secret);
            req.decoded_token = user;
            next();
        } else {
            res.sendStatus(403);
        }
    }
}