var path = require("path");
var fs = require("fs");
var exports = {};
var ext = ".js";
fs.readdirSync(__dirname)
    .filter(function(file) {
        return file.indexOf(ext) !== 0 && file !== 'index.js';
    })
    .forEach(function(file) {
        var name = path.basename(file, ext);
        var abspath = __dirname + "/" + file;
        exports[name] = require(abspath);
    });
module.exports = exports;