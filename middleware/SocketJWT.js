var jwtIO = require('socketio-jwt');
module.exports = function(secret){
    return jwtIO.authorize({
        secret: secret,
        handshake: true
    });
}